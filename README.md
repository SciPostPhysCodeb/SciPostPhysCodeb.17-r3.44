# Codebase release 3.44 for xSPDE

by Simon Kiesewetter, Ria R. Joseph, Peter D. Drummond

SciPost Phys. Codebases 17-r3.44 (2023) - published 2023-10-02

[DOI:10.21468/SciPostPhysCodeb.17-r3.44](https://doi.org/10.21468/SciPostPhysCodeb.17-r3.44)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.17-r3.44) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Simon Kiesewetter, Ria R. Joseph, Peter D. Drummond

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.17-r3.44](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.17-r3.44)
* Live (external) repository at [https://github.com/peterddrummond/xspde_matlab/releases/tag/v3.44](https://github.com/peterddrummond/xspde_matlab/releases/tag/v3.44)
